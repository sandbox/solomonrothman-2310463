INTRODUCTION
------------
This module adds a cron job that checks to see if the site is in maintenance mode and if it is, it takes it out of maintenance mode. Certain errors (notably PDO erros or database unavailable errors) in some cases, can trigger the site to go into maintenance mode. This module ensures that the site will be taken out of maintenance mode when a cron jobs checks and find it in this state. Because most users should not use this, the bad judgement module is listed as it's dependency. It could have a legitmite use, if a site was experincing periodic errors that trigged maintenance mode, this module could be used to bring the site automatically back online, until the root problem is found and solved. Again most users will not need this module so please do not enable it.

* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/solomonrothman/2310463
* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2310463
   
REQUIREMENTS
------------
This module requires the following modules:
 * Disqus (https://www.drupal.org/project/bad_judgement)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Enable the module like any normal module. It has no configuration options. If the module finds the site in maintenance mode, it will log this in the watchdog for review.
 
 MAINTAINERS
 -----------
 Current maintainers:
 * Solomon Rothman - https://www.drupal.org/u/solomonrothman